﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CORE.Controllers;
using CORE.Models;

namespace AppClinic
{
    /// <summary>
    /// Логика взаимодействия для Doctor.xaml
    /// </summary>
    public partial class Doctor : Page
    {
        private DoctorController _doctorController = new DoctorController();

        private List<DoctorModel> _doctor
        {
            get { return _doctorController.GetAll(); }
        }

        public Doctor()
        {
            InitializeComponent();

            listBox.SelectionChanged += (sender, args) =>
            {
                if (!(sender is ListBox listBox1) || listBox1.SelectedItem == null) return;
                int tag = ((DoctorModel)listBox1.SelectedItem).Id;
                
                MessageBoxResult result = MessageBox.Show("Вы уверены что хотите удалить элемент?", "Подтверждение удаления", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    _doctorController.Remove(tag);
                }
                else
                {
                    return;
                }
                listBox.Items.Refresh();
            };
            
            listBox.ItemsSource = _doctor;
            
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var name = __TextBoxName.Text;
            var surname = __TextBoxSurname.Text;
            var middleName = __TextBoxPatronymic.Text;
            var specialization = __TextBoxSpecialty.Text;
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(surname) || string.IsNullOrEmpty(middleName) ||
                string.IsNullOrEmpty(specialization))
            {
                MessageBox.Show("Пожалуйста заполните все поля");
                return;
            }

            var doctor = new DoctorModel
            {
                FirstName = $"{name}",
                LastName = $"{surname}",
                MiddleName = $"{middleName}",
                Specialization = $"{specialization}"
            };

            _doctorController.Create(doctor);

            __TextBoxName.Text = String.Empty;
            __TextBoxSurname.Text = String.Empty;
            __TextBoxPatronymic.Text = String.Empty;
            __TextBoxSpecialty.Text = String.Empty;
            
            listBox.Items.Refresh();
        }
    }
}