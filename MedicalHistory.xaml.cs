using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using CORE.Controllers;
using CORE.Models;

namespace App
{
    public partial class MedicalHistory : Window
    {
        private int Id;
        private AppointmentController _appointmentController = new AppointmentController();

        private List<AppointmentModel> _appointment
        {
            get
            {
                return _appointmentController.GetFirst().OrderBy(a => a.Date).ToList();
                ;
            }
        }

        public MedicalHistory(int id)
        {
            Id = id;
            InitializeComponent();

            listBox.ItemsSource = _appointmentController.GetById(id).MedicalHistory;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(__TextBoxHistoryItem.Text))
            {
                _appointmentController.AddMedicalHistoryById(Id,
                    DateTime.Now.ToString() +" | "+ __TextBoxHistoryItem.Text);
                _appointmentController.Save();
                
                listBox.ItemsSource = _appointmentController.GetById(Id).MedicalHistory;
                listBox.Items.Refresh();
                __TextBoxHistoryItem.Text = String.Empty;
            }
        }
    }
}