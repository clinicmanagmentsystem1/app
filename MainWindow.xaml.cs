﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppClinic
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string[] ButtonNames = new string[] { "Врачи", "Пациенты", "Запись к врачу" };

        object screenDoctor = new AppClinic.Doctor();
        object screenPatient = new AppClinic.Patient();
        object screenAppointment = new AppClinic.Appointment();
        object screenPastPage = new AppClinic.PastPage();

        public MainWindow()
        {
            InitializeComponent();
            frame.Navigate(new AppClinic.HomePage());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            screenAppointment = new AppClinic.Appointment();
            frame.Navigate(screenDoctor);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            screenAppointment = new AppClinic.Appointment();
            frame.Navigate(screenPatient);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            screenAppointment = new AppClinic.Appointment();
            frame.Navigate(screenAppointment);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            screenPastPage = new AppClinic.PastPage();
            frame.Navigate(screenPastPage);
        }
    }
}